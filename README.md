# README #

ReZurection is a 2D shoot-them-up writted in JavaScript.
The main purpose is to produce a Windows native application,
but since JavaScript is supported on all platforms, the project will be easy to port.
Game should be finished in December 2015.

### Team : ###

•	Grégoire CHAMBERLAND,            gregoire.chamberland@etu.udamail.fr
•	Pierre CHARLES,                  pierre.charles@etu.udamail.fr